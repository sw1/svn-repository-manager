(function(angular) {
	angular.module('webui').controller('ProjectsController',
			function($scope, $routeParams, $http, config, $mdDialog) {
				$scope.name = "ProjectsController";
				$scope.params = $routeParams;
				$scope.path = "/";
				
				$scope.getLeafs = function(path){
					$scope.path=path;
						$http.post(config.restURL+'/private/project/get/path/leafs', path).success(function(data) {
							$scope.projects = data;
							
							$scope.path_list=[];
							var subprojects = path.split("/");
							var start_path= "";
							for(var i=1; i< subprojects.length; i++){
								start_path+="/"+subprojects[i];
								$scope.path_list.push({name: subprojects[i], path: start_path});
							}
						});
				}
				$scope.getLeafs("/");
				$scope.addProject = function(event){
					$mdDialog.show({
						      controller: AddProjectDialogController,
						      templateUrl: 'view/ProjectAddDialog.html',
						      parent: angular.element(document.body),
						      clickOutsideToClose:true,
						      targetEvent: event
						      })
						    .then(function(project) {
						    	project.path=$scope.path + "/";
						        $http.post(config.restURL+'/private/project/create', project);
						      });
				}
				
				$scope.remove = function(event, focusedProject){
					$http.post(config.restURL+'/private/project/remove', focusedProject);
				}
				
				$scope.showPermissions = function(event, focusedProject){
					$mdDialog.show({
						      controller: ProjectPermissionDialogController,
						      templateUrl: 'view/ProjectPermissionsDialog.html',
						      parent: angular.element(document.body),
						      clickOutsideToClose:true,
						      targetEvent: event,
						      locals: {$project: focusedProject}
						      })
						    .then(function(project) {
						        $http.put(config.restURL+'/private/project/edit', project);
						      });
				}
				
				var originatorEv;
			    $scope.openMenu = function($mdOpenMenu, ev) {
			      originatorEv = ev;
			      $mdOpenMenu(ev);
			    };
			});
	
function ProjectPermissionDialogController($scope, $mdDialog, $http, config, $project){
		$scope.project=$project;
		$scope.selectedItem=null;
		$scope.searchText="";
		$scope.foundUsers = [];
		$scope.foundGroups = [];

		$scope.querySearchUsers = function(searchText){
			$http.get(config.restURL+'/private/user/get/name/%25'+searchText+'%25/alias/%25'+searchText+'%25').success(function(data) {
				$scope.foundUsers = data;
			});
		}
		
		$scope.querySearchGroups = function(searchText){
			$http.get(config.restURL+'/private/group/get/search/%25'+searchText+'%25').success(function(data) {
				$scope.foundGroups = data;
			});
		}
		
		$scope.getPermissionsAcessType = function(permissions, accessType){
			var filteredPermissions = [];
			for(var i = 0; i < permissions.length;i++){
				if(permissions[i].access == accessType){
					filteredPermissions.push(permissions[i]);
				}
			}
			return filteredPermissions;
		}
		
		$scope.getPermissionsPrincipalType = function(permissions, principalType){
			var filteredPermissions = [];
			for(var i = 0; i < permissions.length;i++){
				if(permissions[i][principalType] != null){
					filteredPermissions.push(permissions[i]);
				}
			}
			return filteredPermissions;
		}
		
		$scope.getPermissionsAcessAndPrincipalType = function(permissions, accessType, principalType){
			var filteredPermissions = [];
			for(var i = 0; i < permissions.length;i++){
				if(permissions[i][principalType] != null && permissions[i].access == accessType){
					filteredPermissions.push(permissions[i][principalType]);
				}
			}
			return filteredPermissions;
		}
		
		$scope.noneUsers=$scope.getPermissionsAcessAndPrincipalType($scope.project.permissions, "NONE", "user");
		$scope.readUsers=$scope.getPermissionsAcessAndPrincipalType($scope.project.permissions, "READ", "user");
		$scope.readWriteUsers=$scope.getPermissionsAcessAndPrincipalType($scope.project.permissions, "READWRITE", "user");
		$scope.noneGroups=$scope.getPermissionsAcessAndPrincipalType($scope.project.permissions, "NONE", "group");
		$scope.readGroups=$scope.getPermissionsAcessAndPrincipalType($scope.project.permissions, "READ", "group");
		$scope.readWriteGroups=$scope.getPermissionsAcessAndPrincipalType($scope.project.permissions, "READWRITE", "group");

		
		$scope.saveProject=function(){
			$scope.project.permissions=[];
			for(var i = 0; i< $scope.noneUsers.length; i++){
				$scope.project.permissions.push({user : $scope.noneUsers[i], access:"NONE"});
			}
			for(var i = 0; i< $scope.readUsers.length; i++){
				$scope.project.permissions.push({user : $scope.readUsers[i], access:"READ"});
			}
			for(var i = 0; i< $scope.readWriteUsers.length; i++){
				$scope.project.permissions.push({user : $scope.readWriteUsers[i], access:"READWRITE"});
			}
			for(var i = 0; i< $scope.noneGroups.length; i++){
				$scope.project.permissions.push({group : $scope.noneGroups[i], access:"NONE"});
			}
			for(var i = 0; i< $scope.readGroups.length; i++){
				$scope.project.permissions.push({group : $scope.readGroups[i], access:"READ"});
			}
			for(var i = 0; i< $scope.readWriteGroups.length; i++){
				$scope.project.permissions.push({group : $scope.readWriteGroups[i], access:"READWRITE"});
			}
			$mdDialog.hide($scope.project);
		}
}
	
function AddProjectDialogController($scope, $mdDialog){
	$scope.project={};
	$scope.addProject=function(){
		
	    $mdDialog.hide($scope.project);
	}
}

	
})(window.angular);
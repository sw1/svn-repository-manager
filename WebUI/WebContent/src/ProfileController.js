(function(angular) {
	'use strict';
	angular.module('webui').controller('ProfileController',
			function($scope, $routeParams, $http, config, $mdDialog) {
				$scope.name = "ProfileController";
				$scope.params = $routeParams;
				$scope.readonly=true;
				$http.get(config.restURL+'/private/user/get/current').success(function(data) {
					$scope.user = data;
					$scope.getGroup($scope.user.name);
				});
				$scope.getGroup = function(username){
					$http.get(config.restURL+'/private/group/get/names/'+username).then(function(response) {
						$scope.groups=response.data;
					})
				}
				
				$scope.saveUser = function(){
					$http.put(config.restURL+'/private/user/edit',$scope.user).success(function(data) {
						$scope.status = "User changed";
						$mdDialog.show(
							      $mdDialog.alert()
							        .parent(angular.element(document.querySelector('#changeUserForm')))
							        .clickOutsideToClose(true)
							        .title('User changed')
							        .content('User has been changed')
							        .ariaLabel('User changed')
							        .ok('ok')
							    );
					}).error(function(data) {
						$scope.status = "User change failed "+data;
						$mdDialog.show(
							      $mdDialog.alert()
							        .parent(angular.element(document.querySelector('#changeUserForm')))
							        .clickOutsideToClose(true)
							        .title('User change failed')
							        .content('User has not been changed!')
							        .ariaLabel('User change failed')
							        .ok('ok')
							    );
					});
				}
			});

})(window.angular);
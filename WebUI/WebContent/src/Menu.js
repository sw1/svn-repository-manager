(function(){

  angular.module('webui', [ 'ngMaterial','ngRoute','web.settings', 'md.data.table','ng']).config(function($mdIconProvider) {
	  $mdIconProvider
	    .defaultIconSet('mdi.svg')
	});
 
  angular.module('webui').directive('ngRightClick', function($parse) {
	    return function(scope, element, attrs) {
	        var fn = $parse(attrs.ngRightClick);
	        element.bind('contextmenu', function(event) {
	            scope.$apply(function() {
	                event.preventDefault();
	                fn(scope, {$event:event});
	            });
	        });
	    };
	});
  
  angular.module('webui').config( function($httpProvider) {
	  $httpProvider.interceptors.push(function($injector, $rootScope) {
	    return {
	      responseError: function(rejection) {
	    	  if(rejection.status == 401){
	    		  $rootScope.$broadcast( 'authFail', { message: 'An unexpected error has occurred. Please try again.' } );
	    	  }	
	    	return rejection;
	          
	      }

	    };
	  });
	});
  
  angular.module('webui').run(function($http) {
	  $http.defaults.headers.common['Authorization'] = 'Basic ' + window.btoa("username" + ':' + "password");
  });

	angular.module('webui').config(function($routeProvider) {
		$routeProvider.when('/profile', {
			templateUrl : 'view/ProfileView.html',
			controller : 'ProfileController'
		}).when('/users', {
			templateUrl : 'view/UsersView.html',
			controller : 'UsersController'
		}).when('/groups', {
			templateUrl : 'view/GroupsView.html',
			controller : 'GroupsController'
		}).when('/projects', {
			templateUrl : 'view/ProjectsView.html',
			controller : 'ProjectsController'
		}).when('/repository', {
			templateUrl : 'view/RepositoryView.html',
			controller : 'RepositoryController'
		}).when('/reset', {
			templateUrl : 'view/ResetView.html',
			controller : 'ResetController'
		}).otherwise({
	        redirectTo: '/profile'
	      });

	});
	
	angular.module('webui').config(function($mdThemingProvider, $mdIconProvider){

            $mdThemingProvider.theme('default')
                .primaryPalette('green')
                .accentPalette('red');

    });
	
	
})();

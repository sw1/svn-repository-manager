(function(angular) {
	'use strict';
	angular.module('webui').controller('RepositoryController',
			function($scope, $routeParams, $http, config, $mdDialog) {
				$scope.name = "RepositoryController";
				$scope.params = $routeParams;

				$http.get(config.restURL+'/private/config/get/repo').success(function(response) {
					$scope.repo = response;

				});
				$http.get(config.restURL+'/private/config/get/server').success(function(response) {
					$scope.server = response;
				});
				$scope.saveSettings = function(){
					
				}
				
				$scope.saveRepoConfig = function (){
					$http.put(config.restURL+'/private/config/put/repo', $scope.repo).success(function(data) {
						$mdDialog.show(
							      $mdDialog.alert()
							        .parent(angular.element(document.querySelector('#changeUserForm')))
							        .clickOutsideToClose(true)
							        .title('Repository config saved')
							        .content('Repository config saved')
							        .ariaLabel('Repository config saved')
							        .ok('ok')
							    );
					}).error(function(data) {
						$mdDialog.show(
							      $mdDialog.alert()
							        .parent(angular.element(document.querySelector('#changeUserForm')))
							        .clickOutsideToClose(true)
							        .title('Repository config save failed')
							        .content('Repository config has not been changed!')
							        .ariaLabel('Repository config change failed')
							        .ok('ok')
							    );
					});
				}
				
				$scope.saveServerConfig = function (){
					$http.put(config.restURL+'/private/config/put/server', $scope.server).success(function(data) {
						$mdDialog.show(
							      $mdDialog.alert()
							        .parent(angular.element(document.querySelector('#changeUserForm')))
							        .clickOutsideToClose(true)
							        .title('Server config saved')
							        .content('Server config saved')
							        .ariaLabel('Server config saved')
							        .ok('ok')
							    );
					}).error(function(data) {
						$mdDialog.show(
							      $mdDialog.alert()
							        .parent(angular.element(document.querySelector('#changeUserForm')))
							        .clickOutsideToClose(true)
							        .title('Server config save failed')
							        .content('Server config has not been changed!')
							        .ariaLabel('Server config change failed')
							        .ok('ok')
							    );
					});
				}
				
			});

})(window.angular);
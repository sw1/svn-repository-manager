/**
 * 
 */
(function(angular) {
	  'use strict';
	angular.module('webui').controller('MainController', function($scope,$route,$routeParams, $location) {
		$scope.$route = $route;
		$scope.$location = $location;
		$scope.$routeParams = $routeParams;
	    }
	);
	
	})(window.angular);


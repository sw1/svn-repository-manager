(function(angular) {
	'use strict';
	angular.module('webui').controller('UsersController',
			function($scope, $routeParams, $http, config, $mdDialog) {
				$scope.name = "UsersController";
				$scope.params = $routeParams;
				$scope.groups = {};
				$http.get(config.restURL+'/private/user/get/all').success(function(data) {
					$scope.users = data;
					for	(var index = 0; index < $scope.users.length; index++) {
						$scope.getGroups($scope.users[index].name);
					} 
				});
				
				$scope.getGroups = function(username){
					$http.get(config.restURL+'/private/group/get/names/'+username).then(function(response) {
						$scope.groups[username]=response.data;
					});
				};
				
				$scope.remove = function(user){
					$http.post(config.restURL+'/private/user/remove', user);
				}
				
				$scope.addUser = function(event){
					$mdDialog.show({
					      controller: AddUserDialogController,
					      templateUrl: 'view/ProfileView.html',
					      parent: angular.element(document.body),
					      clickOutsideToClose:true,
					      targetEvent: event
					      })
					    .then(function(credentials) {
					    	$http.post(config.restURL+'/private/user/create',credentials.user);
					    	for(var index = 0; index<credentials.groups.length ; index++){
					    		$http.post(config.restURL+"/private/group/add/user/"+credentials.groups[index],credentials.user);
					    	}
					    	
					});
				
				}
				$scope.edit = function(event, focusedUser){
					$mdDialog.show({
						      controller: UsersEditDialogController,
						      templateUrl: 'view/UserEditDialog.html',
						      parent: angular.element(document.body),
						      clickOutsideToClose:true,
						      targetEvent: event,
						      locals: {$user: focusedUser}
						      })
						      .then(function(credentials) {
						    	$http.put(config.restURL+'/private/user/edit',credentials.user);
						    	for(var index = 0; index<credentials.groups.length ; index++){
						    		$http.post(config.restURL+"/private/group/add/user/"+credentials.groups[index].name,credentials.user);
						    	}
						      });
				}
				var originatorEv;
			    $scope.openMenu = function($mdOpenMenu, ev) {
			      originatorEv = ev;
			      $mdOpenMenu(ev);
			    };
				
					
				  $scope.selected = [];

				  $scope.query = {
				    filter: '',
				    order: 'name',
				    limit: 50,
				    page: 1
				  };

				  function success(desserts) {
				    $scope.desserts = desserts;
				  }

				  // in the future we may see a few built in alternate headers but in the mean time
				  // you can implement your own search header and do something like
				  $scope.search = function (predicate) {
				    $scope.filter = predicate;
				    $scope.deferred = $nutrition.desserts.get($scope.query, success).$promise;
				  };

				  $scope.onOrderChange = function (order) {
				    return $nutrition.desserts.get($scope.query, success).$promise; 
				  };

				  $scope.onPaginationChange = function (page, limit) {
				    return $nutrition.desserts.get($scope.query, success).$promise; 
				  };
			});
	
function UsersEditDialogController($scope, $mdDialog, $http, config, $user){
	$scope.user=$user;
	$scope.groups = [];
	$scope.selectedItem=null;
	$scope.searchText="";
	$scope.foundGroups = [];

	$scope.querySearchGroups = function(searchText){
		$http.get(config.restURL+'/private/group/get/search/%25'+searchText+'%25').success(function(data) {
			$scope.foundGroups = data;
		});
	}
	
	$scope.saveUser=function(){
		$mdDialog.hide({"user": $scope.user, "groups":$scope.groups});
	}
}
		
function AddUserDialogController($scope, $mdDialog){
	$scope.readonly = false;
	$scope.user={};
	$scope.groups=[];
	$scope.user.roles=[];
	$scope.saveUser = function() {
	    $mdDialog.hide({"user": $scope.user, "groups":$scope.groups});
	  };
	
}
	
})(window.angular);
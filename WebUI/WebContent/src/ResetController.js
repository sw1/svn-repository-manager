(function(angular) {
	'use strict';
	angular.module('webui').controller('ResetController',
			function($scope, $routeParams, $http, config, $mdDialog) {
				$scope.name = "ResetController";
				$scope.params = $routeParams;
				$scope.user = {"name": "","mail" : "","password" : null, "key" : null, "token" : $routeParams.token};
				$scope.status = "";
				$scope.sendResetToken = function(){
					$http.post(config.restURL+'/public/auth/reset/sendToken',$scope.user).success(function(data) {
						$scope.status = "Email sent";
						$mdDialog.show(
							      $mdDialog.alert()
							        .parent(angular.element(document.querySelector('#tokenForm')))
							        .clickOutsideToClose(true)
							        .title('Email Sent')
							        .content('Email with token was send')
							        .ariaLabel('Email send')
							        .ok('ok')
							    );
					}).error(function(data) {
						$scope.status = "Error sending mail "+data;
						$mdDialog.show(
							      $mdDialog.alert()
							        .parent(angular.element(document.querySelector('#tokenForm')))
							        .clickOutsideToClose(true)
							        .title('Error send mail')
							        .content('Email with token was  not send!')
							        .ariaLabel('Email not send')
							        .ok('ok')
							    );
					});
				}
				
				$scope.changePassword = function(){
					$http.post(config.restURL+'/public/auth/reset/changeUser',$scope.user).success(function(data) {
						$scope.status = "Password changed";
						$mdDialog.show(
							      $mdDialog.alert()
							        .parent(angular.element(document.querySelector('#changeUserForm')))
							        .clickOutsideToClose(true)
							        .title('Password changed')
							        .content('Your password has been changed')
							        .ariaLabel('Password changed')
							        .ok('ok')
							    );
					}).error(function(data) {
						$scope.status = "Password change failed "+data;
						$mdDialog.show(
							      $mdDialog.alert()
							        .parent(angular.element(document.querySelector('#changeUserForm')))
							        .clickOutsideToClose(true)
							        .title('Password change failed')
							        .content('Your password has not been changed!')
							        .ariaLabel('Password change failed')
							        .ok('ok')
							    );
					});
				}
			});

})(window.angular);
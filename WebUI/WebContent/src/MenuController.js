(function() {
	angular.module('webui').controller(
			'MenuController',
			function($scope, $http, $mdSidenav, $mdBottomSheet, $log, $q,
					$route, $routeParams, $location, $rootScope, $mdDialog, $mdMedia) {
				$scope.$route = $route;
				$scope.$location = $location;
				$scope.$routeParams = $routeParams;
				$http.get('src/menu.json').success(function(data) {
					$scope.sections = data;
					$scope.active = data[0];
				});
				$scope.active = null;
				$scope.isActive = function isActive(section) {
					if (section === $scope.active) {
						return true;
					} else {
						return false;
					}
				}
				$scope.activate = function(section) {
					$scope.active = section;
				}

				$scope.toggleMenu = function() {
					var pending = $mdBottomSheet.hide() || $q.when(true);

					pending.then(function() {
						$mdSidenav('left').toggle();
					});
				}
				
				$rootScope.$on("authFail", function (event, eventData) {
			    	$mdDialog.show({
					      controller: DialogController,
					      templateUrl: 'view/LoginForm.html',
					      parent: angular.element(document.body),
					      clickOutsideToClose:true,
					      targetEvent: event
					      })
					    .then(function(credentials) {
					        $http.defaults.headers.common['Authorization'] = 'Basic ' + window.btoa( credentials.username+ ':' + credentials.password);
					      });
			    });
				
				$scope.logout = function(){
			        $http.defaults.headers.common['Authorization'] = 'Basic ' + window.btoa( "username"+ ':' + "password");
				}
				

				
			});
	
	
	
})();

function DialogController($scope, $mdDialog) {
	$scope.username="adam";
	$scope.password="passs";
	  $scope.cancel = function() {
	    $mdDialog.cancel();
	  };
	  $scope.login = function() {
	    $mdDialog.hide({"username": $scope.username, "password":$scope.password});
	  };
	}


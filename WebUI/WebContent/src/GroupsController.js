(function(angular) {
	'use strict';
	angular.module('webui').controller('GroupsController',
			function($scope, $routeParams, $http, config, $mdDialog) {
				$scope.name = "GroupsController";
				$scope.params = $routeParams;
				
				$http.get(config.restURL+'/private/group/get/all').success(function(data) {
					$scope.groups = data;
				});
				
				$scope.remove = function(event, group){
					$http.post(config.restURL+'/private/group/remove', group);
				}
				
				$scope.addGroup = function(event){
					$mdDialog.show({
						      controller: AddGroupDialogController,
						      templateUrl: 'view/GroupAddDialog.html',
						      parent: angular.element(document.body),
						      clickOutsideToClose:true,
						      targetEvent: event
						      })
						    .then(function(group) {
						        $http.post(config.restURL+'/private/group/create', group);
						      });
				}
				
				$scope.showUsers = function(event, focusedGroup){
					$mdDialog.show({
						      controller: GroupUsersDialogController,
						      templateUrl: 'view/GroupUsersDialog.html',
						      parent: angular.element(document.body),
						      clickOutsideToClose:true,
						      targetEvent: event,
						      locals: {$group: focusedGroup}
						      })
						    .then(function(group) {
						        $http.put(config.restURL+'/private/group/edit', group);
						      });
				}
				var originatorEv;
			    $scope.openMenu = function($mdOpenMenu, ev) {
			      originatorEv = ev;
			      $mdOpenMenu(ev);
			    };
				
			});
	
function GroupUsersDialogController($scope, $mdDialog, $http, config, $group){
	$scope.group=$group;
	$scope.selectedItem=null;
	$scope.searchText="";
	$scope.foundUsers = [];

	$scope.querySearchUsers = function(searchText){
		$http.get(config.restURL+'/private/user/get/name/%25'+searchText+'%25/alias/%25'+searchText+'%25').success(function(data) {
			$scope.foundUsers = data;
		});
	}
	
	$scope.saveGroup=function(){
		$mdDialog.hide($scope.group);
	}
}
	
function AddGroupDialogController($scope, $mdDialog){
	$scope.group={};
	$scope.group.users=[];
	$scope.addGroup=function(){
	    $mdDialog.hide($scope.group);
	}
}

})(window.angular);
(function(angular) {
	'use strict';
	angular.module('webui').controller('RepositoryController',
			function($scope, $routeParams, $http, config) {
				$scope.name = "RepositoryController";
				$scope.params = $routeParams;

				$http.get(config.restURL+'/config/get/repo').success(function(response) {
					$scope.repo = response;

				});
				$http.get(config.restURL+'/config/get/server').success(function(response) {
					$scope.server = response;
				});
				$scope.$on('event:auth-loginRequired', function(event, data) {
				  	  
				  	  alert("Problem");
					});
			});

})(window.angular);
(function(angular) {
	'use strict';
	angular.module('webui').controller('ProjectsController',
			function($scope, $routeParams, $http, config) {
				$scope.name = "ProjectsController";
				$scope.params = $routeParams;
				
				$http.get(config.restURL+'/project/get/all').success(function(data) {
					$scope.prjs = data;
				});
			});

	
})(window.angular);
(function(angular) {
	'use strict';
	angular.module('webui').controller('ProfileController',
			function($scope, $routeParams, $http, config) {
				$scope.name = "ProfileController";
				$scope.params = $routeParams;
				
				$http.get(config.restURL+'/user/get/current').success(function(data) {
					$scope.user = data;
				});
			});

})(window.angular);
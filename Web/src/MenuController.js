(function() {
	angular.module('webui').controller(
			'MenuController',
			function($scope, $http, $mdSidenav, $mdBottomSheet, $log, $q,
					$route, $routeParams, $location) {
				$scope.$route = $route;
				$scope.$location = $location;
				$scope.$routeParams = $routeParams;
				$http.get('src/menu.json').success(function(data) {
					$scope.sections = data;
					$scope.active = data[0];
				});
				$scope.active = null;
				$scope.isActive = function isActive(section) {
					if (section === $scope.active) {
						return true;
					} else {
						return false;
					}
				}
				$scope.activate = function(section) {
					$scope.active = section;
				}

				$scope.toggleMenu = function() {
					var pending = $mdBottomSheet.hide() || $q.when(true);

					pending.then(function() {
						$mdSidenav('left').toggle();
					});
				}

				
			});
	
})();
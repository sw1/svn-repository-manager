(function(angular) {
	'use strict';
	angular.module('webui').controller('GroupsController',
			function($scope, $routeParams, $http, config) {
				$scope.name = "GroupsController";
				$scope.params = $routeParams;
				
				$http.get(config.restURL+'/group/get/all').success(function(data) {
					$scope.groups = data;
				});
			});

})(window.angular);
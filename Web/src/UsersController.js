(function(angular) {
	'use strict';
	angular.module('webui').controller('UsersController',
			function($scope, $routeParams, $http, config) {
				$scope.name = "UsersController";
				$scope.params = $routeParams;
				
				$http.get(config.restURL+'/user/get/all').success(function(data) {
					$scope.users = data;
				});
				  $scope.selected = [];

				  $scope.query = {
				    filter: '',
				    order: 'name',
				    limit: 50,
				    page: 1
				  };

				  function success(desserts) {
				    $scope.desserts = desserts;
				  }

				  // in the future we may see a few built in alternate headers but in the mean time
				  // you can implement your own search header and do something like
				  $scope.search = function (predicate) {
				    $scope.filter = predicate;
				    $scope.deferred = $nutrition.desserts.get($scope.query, success).$promise;
				  };

				  $scope.onOrderChange = function (order) {
				    return $nutrition.desserts.get($scope.query, success).$promise; 
				  };

				  $scope.onPaginationChange = function (page, limit) {
				    return $nutrition.desserts.get($scope.query, success).$promise; 
				  };
			});

})(window.angular);
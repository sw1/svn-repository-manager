(function(){

  angular.module('webui', [ 'ngMaterial','ngRoute','web.settings', 'md.data.table','ng']).config(function($mdIconProvider) {
	  $mdIconProvider
	    .defaultIconSet('mdi.svg')
	});
  angular.module('webui').config(function($httpProvider) {
	  $httpProvider.interceptors.push(function() {
	    return {
	      response: function(res) {
	        /* This is the code that transforms the response. `res.data` is the
	         * response body */
	        return res;
	      },
	      responseError: function(rejection) {
	          alert("res "+rejection.status + rejection.headers);
	      },
	      requestError:function(rejection) {
	          alert("req "+rejection.status);
	      }

	    };
	  });
	});

	angular.module('webui').config(function($routeProvider) {
		$routeProvider.when('/profile', {
			templateUrl : 'view/ProfileView.html',
			controller : 'ProfileController'
		}).when('/users', {
			templateUrl : 'view/UsersView.html',
			controller : 'UsersController'
		}).when('/groups', {
			templateUrl : 'view/GroupsView.html',
			controller : 'GroupsController'
		}).when('/projects', {
			templateUrl : 'view/ProjectsView.html',
			controller : 'ProjectsController'
		}).when('/repository', {
			templateUrl : 'view/RepositoryView.html',
			controller : 'RepositoryController'
		}).otherwise({
	        redirectTo: '/profile'
	      });

	});
	
	angular.module('webui').config(function($mdThemingProvider, $mdIconProvider){

            $mdThemingProvider.theme('default')
                .primaryPalette('green')
                .accentPalette('red');

    });
	
	angular.module('webui').controller('Controller', function($scope, $mdDialog, $mdMedia) {
		  $scope.format = 'M/d/yy h:mm:ss a';
		  $scope.$on('event:auth-loginRequired', function() {
        	  
        	  alert("Problem");
        	  
        	  $mdDialog.show({
    		      controller: DialogController,
    		      templateUrl: 'dialog1.tmpl.html',
    		      parent: angular.element(document.body),
    		      targetEvent: ev,
    		      clickOutsideToClose:true,
    		      fullscreen: $mdMedia('sm') && $scope.customFullscreen
    		    })
    		    .then(function(answer) {
    		      $scope.status = 'You said the information was "' + answer + '".';
    		    }, function() {
    		      $scope.status = 'You cancelled the dialog.';
    		    });
    		    $scope.$watch(function() {
    		      return $mdMedia('sm');
    		    }, function(sm) {
    		      $scope.customFullscreen = (sm === true);
    		    });
          });
	})
	.directive('authDemoApplication', function() {
	    return {
	        restrict: 'C',
	        link: function(scope, elem, attrs) {
	     
	          scope.$on('event:auth-loginRequired', function(event, data) {
	        	  
	        	  alert("Problem");
	        	  
	        	  $mdDialog.show({
        		      controller: DialogController,
        		      templateUrl: 'dialog1.tmpl.html',
        		      parent: angular.element(document.body),
        		      targetEvent: ev,
        		      clickOutsideToClose:true,
        		      fullscreen: $mdMedia('sm') && $scope.customFullscreen
        		    })
        		    .then(function(answer) {
        		      $scope.status = 'You said the information was "' + answer + '".';
        		    }, function() {
        		      $scope.status = 'You cancelled the dialog.';
        		    });
        		    $scope.$watch(function() {
        		      return $mdMedia('sm');
        		    }, function(sm) {
        		      $scope.customFullscreen = (sm === true);
        		    });
	          });
	          scope.$on('event:auth-loginConfirmed', function() {
	           
	          });
	        }
	      }
	    });
})();

package service;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import beans.GroupManagerLocal;
import beans.GroupManagerRemote;
import permissions.Group;
import permissions.User;

@Path("private/group")
public class GroupService {
	@EJB
	private GroupManagerRemote groupManager;
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/name/{name}")
	public Group getGroup(@PathParam("name") String name){
		return groupManager.getGroup(name);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/search/{name}")
	public List<Group> searchGroup(@PathParam("name") String name){
		return groupManager.searchGroup(name);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/all")
	public List<Group> getAllGroups(){
		return groupManager.getGroups();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/user/{name}")
	public List<Group> getUserGroups(@PathParam("name") String user){
		return groupManager.getUserGroups(user);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/names/{name}")
	public List<String> getUserGroupsNames(@PathParam("name") String user){
		return groupManager.getUserGroupsNames(user);
	}
		
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("edit")
	public void editGroup(Group group){
		groupManager.mergeGroup(group);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("create")
	public void createGroup(Group group){
		groupManager.createGroup(group);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("remove")
	public void removeGroup(Group group){
		groupManager.removeGroup(group);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("add/user/{group}")
	public void addUserToGroup(User user, @PathParam("group") String group){
		groupManager.addUserToGroup(user, group);
	}
}

package service;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.tmatesoft.svn.core.SVNException;

import beans.ProjectManagerLocal;
import beans.ProjectManagerRemote;
import permissions.Project;

@Path("private/project")
public class ProjectService {
	@EJB
	private ProjectManagerRemote projectManager;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/path")
	public Response getProject(String path) throws SVNException {
		ResponseBuilder responseBuilder;
		try{
			Project project = projectManager.getProject(path);
			responseBuilder = Response.ok(project);
		}catch(SVNException e){
			responseBuilder = Response.serverError().entity(e);
		}
		return responseBuilder.build();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/path/leafs")
	public Response getProjectLeafs(String path) throws SVNException {
		ResponseBuilder responseBuilder;
		try{
			List<Project> project = projectManager.getProjectsInPath(path);
			responseBuilder = Response.ok(project);
		}catch(SVNException e){
			responseBuilder = Response.serverError().entity(e);
		}
		return responseBuilder.build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("create")
	public Response createProject(Project project) throws SVNException {
		ResponseBuilder responseBuilder;
		try {
			projectManager.createProject(project);
			responseBuilder = Response.ok(project);

		} catch (SVNException e) {
			responseBuilder = Response.serverError().entity(e);
		}
		return responseBuilder.build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("remove")
	public Response removeeProject(Project project) throws SVNException {
		ResponseBuilder responseBuilder;
		try {
			projectManager.removeProject(project);
			responseBuilder = Response.ok(project);

		} catch (SVNException e) {
			responseBuilder = Response.serverError().entity(e);
		}
		return responseBuilder.build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("edit")
	public Response editProject(Project project) throws SVNException {
		ResponseBuilder responseBuilder;
		try {
			projectManager.mergeProject(project);
			responseBuilder = Response.ok(project);

		} catch (SVNException e) {
			responseBuilder = Response.serverError().entity(e);
		}
		return responseBuilder.build();
	}
}

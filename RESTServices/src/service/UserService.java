package service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.Encoded;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.UserManagerLocal;
import beans.UserManagerRemote;
import exceptions.BadTokenException;
import exceptions.NoUserException;
import permissions.User;

@Path("private/user")
public class UserService {

	@EJB
	private UserManagerRemote userManager;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/current")
	public User getCurrentUser() {
		return userManager.getCurrentUser();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/name/{name}")
	public User getUser(@PathParam("name") String name) {
		return userManager.getUser(name);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/name/{name}/alias/{alias}")
	public List<User> getUserOrAlias(@PathParam("name") String name, @PathParam("alias") String alias) {
		return userManager.getUserByCriterium(name, alias);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/all")
	public List<User> getAllUsers() {
		return userManager.getUsers();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("edit")
	public void edit(User user) {
		try {
			userManager.mergeUser(user);
		} catch (FileNotFoundException e) {

		} catch (IOException e) {

		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("create")
	public void merge(User user) {
		try {
			userManager.createUser(user);
		} catch (FileNotFoundException e) {

		} catch (IOException e) {

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("remove")
	public void remove(User user) {
		userManager.remove(user);
	}
	

}

package service;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.UserManagerRemote;
import exceptions.BadTokenException;
import exceptions.NoUserException;
import permissions.User;

@Path("public/auth")
public class AuthService {

	@EJB
	private UserManagerRemote userManager;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("reset/sendToken")
	public Response reset(User user){
		try {
			userManager.sendResetToken(user);
		} catch (NoUserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(406).build();
		}
		return Response.status(200).build();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("reset/changeUser")
	public Response resetToken(User user){
		try {
			userManager.mergeResetUser(user);
		} catch (BadTokenException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return Response.status(406).build();
		}
		return Response.status(200).build();
	}
}

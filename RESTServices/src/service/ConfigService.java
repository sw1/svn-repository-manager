package service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;

import beans.ConfigManagerLocal;
import beans.ConfigManagerRemote;
import config.NoConfigurationException;
import config.RepositoryConfig;
import config.ServerConfig;

@Path("private/config")
public class ConfigService {
	@EJB
	private ConfigManagerRemote configManager;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/server")
	public ServerConfig getServerConfig(){
		return configManager.getServerConfig();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("get/repo")
	public RepositoryConfig getRepositoryConfig(){
		return configManager.getRepositoryConfig();
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("put/server")
	public void saveServerConfig(ServerConfig serverConfig){
		try {
			configManager.saveAppConfig(serverConfig);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("put/repo")
	public void saveRepositoryConfig(RepositoryConfig repositoryConfig){
		try {
			configManager.saveSVNConfig(repositoryConfig);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

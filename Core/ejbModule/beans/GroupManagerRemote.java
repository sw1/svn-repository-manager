package beans;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;

import permissions.Group;
import permissions.User;

@Remote
public interface GroupManagerRemote {
	
	public Group getGroup(String name);
	public List<Group> getGroups();
	public List<Group> getUserGroups(String username);
	public void createGroup(Group group);
	public void mergeGroup(Group group);
	public void storeGroups(List<Group> groups);
	public List<String> getUserGroupsNames(String username);
	void mergeGroups(List<Group> groups);
	public void addUserToGroup(User user, String groupName);
	public List<Group> searchGroup(String name);
	public void removeGroup(Group group);
}

package beans;

import java.util.List;

import javax.ejb.Local;

import permissions.Group;

@Local
public interface GroupManagerLocal {
	
	public Group getGroup(String name);
	public List<Group> getGroups();
	public List<Group> getUserGroups(String username);
	public void createGroup(Group group);
	public void mergeGroup(Group group);
	public void storeGroups(List<Group> groups);
	public List<String> getUserGroupsNames(String username);
	void mergeGroups(List<Group> groups);
	public void mergeStartupGroups(List<Group> groups);
}

package beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import org.jboss.logging.Logger;

import config.RepositoryManager;
import permissions.Group;
import permissions.User;

@Stateless
@LocalBean
public class GroupManager implements GroupManagerLocal, GroupManagerRemote {

	@Resource
	public UserTransaction utx;
	@PersistenceUnit
	public EntityManagerFactory factory;

	@PersistenceContext
	private EntityManager em;
	
	@Inject
	private RepositoryManager repositoryManager;
	
	private static final Logger LOGGER = Logger.getLogger(ProjectManager.class);
	
	public void addUsertoGroup(User u, String group) {

	}

	@Override
	public Group getGroup(String name) {
		Query q = em.createNamedQuery("selectGroup").setParameter("name", name);
		Group g = null;
		try{
			g = (Group) q.getSingleResult();
		}
		catch(NoResultException e){
			LOGGER.info("No group \""+name+"\" found in db");
		}
		return g;
	}
	
	@Override
	public List<Group> searchGroup(String name) {
		Query q = em.createNamedQuery("selectGroup").setParameter("name", name);
		@SuppressWarnings("unchecked")
		List<Group> g =  q.getResultList();
		return g;
	}

	@Override
	public List<Group> getGroups() {
		Query q = em.createNamedQuery("selectAllGroups");
		@SuppressWarnings("unchecked")
		List<Group> g = q.getResultList();
		return g;
	}

	@RolesAllowed("admin")
	@Override
	public void createGroup(Group group) {
		em.persist(group);
		repositoryManager.getGroups().add(group);
		repositoryManager.writeAuthz();
	}
	
	@RolesAllowed("admin")
	@Override
	public void storeGroups(List<Group> groups) {
		for(Group g: groups){
			createGroup(g);
		}
	}

	@RolesAllowed("admin")
	@Override
	public void mergeGroup(Group group) {
		Group dbGroup = getGroup(group.getName());
		dbGroup.setUsers(group.getUsers());
		group.setId(group.getId());
		em.merge(dbGroup);
	}
	
	@RolesAllowed("admin")
	@Override
	public void mergeGroups(List<Group> groups) {
		for(Group g: groups){
			mergeGroup(g);
		}
	}
	
	public void mergeStartupGroups(List<Group> groups) {
		for(Group group: groups){
			Group dbGroup = getGroup(group.getName());
			if(dbGroup!=null){
				dbGroup.setUsers(group.getUsers());
				group.setId(dbGroup.getId());
				em.merge(dbGroup);
			}else{
				em.merge(group);
			}
			
		}
	}

	@Override
	public List<Group> getUserGroups(String username) {
		Query q = em.createNamedQuery("selectUserGroups").setParameter("name", username);
		@SuppressWarnings("unchecked")
		List<Group> g = q.getResultList();
		return g;
	}
	
	public List<String> getUserGroupsNames(String username) {
		Query q = em.createNamedQuery("selectUserGroupsNames").setParameter("name", username);
		@SuppressWarnings("unchecked")
		List<String> names = q.getResultList();
		return names;
	}
	
	public void addUserToGroup(User user, String groupName){
		Group g = getGroup(groupName);
		if(g.getUsers()==null){
			g.setUsers(new ArrayList<User>());
		}
		g.getUsers().add(user);
		em.merge(g);
	}

	@Override
	public void removeGroup(Group group) {
		em.remove(getGroup(group.getName()));
		repositoryManager.getGroups().remove(group);
	}
}

package beans;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.logging.Logger;
import org.tmatesoft.svn.core.SVNException;

import config.ProjectManagerImpl;
import config.RepositoryManager;
import permissions.Project;

@Stateless
@LocalBean
public class ProjectManager implements ProjectManagerLocal, ProjectManagerRemote {

	@PersistenceContext
	private EntityManager em;
	@Inject
	private ProjectManagerImpl prjMgr;
	@Inject
	private RepositoryManager repositoryManager;
	private static final Logger LOGGER = Logger.getLogger(ProjectManager.class);
	
	@Override
	public List<Project> getProjectByCriterium(String path, String name) throws SVNException {
		return prjMgr.getProjectByCriterium(path, name);
	}

	@RolesAllowed("admin")
	@Override
	public void createProject(Project project) throws SVNException {
		project=mergeSVNProject(project);
		prjMgr.createProject(project);
	}
	
	@RolesAllowed("admin")
	public void removeProject(Project project) throws SVNException{
		prjMgr.removeProject(project);
		
		em.remove(getProjectById(project.getId()));
	}
	
	public void mergeProject(Project project) throws SVNException {
		Project dbProject = null;
		if(project.getId()!=null){
			dbProject = getProjectById(project.getId());
			dbProject.setName(project.getName());
		}else{
			dbProject = getDBProject(project.getPath()+project.getName());
		}
		if(dbProject!=null){
			dbProject.setManagers(project.getManagers());
			em.merge(dbProject);
		}else{
			em.merge(project);
		}
		Project svnProject = getProject(project.getPath());
		svnProject.setManagers(project.getManagers());
		svnProject.setName(project.getName());
		svnProject.setPermissions(project.getPermissions());

		repositoryManager.writeAuthz();

	}
	
	public Project mergeSVNProject(Project project) throws SVNException {
		Project dbProject = getDBProject(project.getPath()+project.getName());
		if(dbProject!=null){
			project.setManagers(dbProject.getManagers());
			project.setId(dbProject.getId());
		}else{
			project = em.merge(project);
		}
		return project;
	}

	@Override
	public List<Project> getProjectsInPath(String path) throws SVNException {
		if (!path.startsWith("/")) {
			path = "/" + path;
		}
		return prjMgr.getProject(path).getLeafs();
	}

	public Project getDBProject(String path)  {
		if (!path.startsWith("/")) {
			path = "/" + path;
		}
		System.out.println(";"+path.substring(0, path.lastIndexOf("/") + 1)+";"+path.substring(path.lastIndexOf("/") + 1, path.length())+";");
		Query q = em.createNamedQuery("selectProjectPathName")
				.setParameter("path", path.substring(0, path.lastIndexOf("/") + 1))
				.setParameter("name", path.substring(path.lastIndexOf("/") + 1, path.length()));
		Project dbProject = null;
		try{
			dbProject = (Project) q.getSingleResult();
		}
		catch(NoResultException e){
			LOGGER.info("No project \""+path+"\" found in db");
		}
		return dbProject;
	}
	
	@Override
	public Project getProject(String path) throws SVNException {
		if (!path.startsWith("/")) {
			path = "/" + path;
		}
		Project	svnProject=prjMgr.getProject(path);
		
		return svnProject;
	}
	
	public Project getProjectById(Long id) throws SVNException {
		Query q = em.createNamedQuery("selectProjectById")
				.setParameter("id", id);
		Project dbProject = (Project) q.getSingleResult();
		return dbProject;
	}

}

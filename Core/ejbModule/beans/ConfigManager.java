package beans;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.xml.bind.JAXBException;

import org.ini4j.InvalidFileFormatException;

import config.LogReader;
import config.NoConfigurationException;
import config.RepositoryConfig;
import config.RepositoryManager;
import config.ServerConfig;

/**
 * Session Bean implementation class ConfigManager
 */
@RolesAllowed("admin")
@Stateless
@LocalBean
public class ConfigManager implements ConfigManagerLocal, ConfigManagerRemote {

	@Inject
	private RepositoryManager repositoryManager;
	
	private LogReader logReader;

    public ConfigManager() {
    }

	@Override
	public void readAppConfig() throws IOException, NoConfigurationException, JAXBException {
		repositoryManager.readAppConfig();
		
	}

	@Override
	public void readSVNConfig() throws InvalidFileFormatException, IOException {
		repositoryManager.readSVNConfig();
	}

	@Override
	public void saveSVNConfig(RepositoryConfig repositoryConfig) throws FileNotFoundException, UnsupportedEncodingException {
		repositoryManager.setRepositoryConfig(repositoryConfig);
		repositoryManager.writeSVNConfig();
		
	}

	@Override
	public void saveAppConfig(ServerConfig serverConfig) throws IOException, NoConfigurationException, JAXBException {
		repositoryManager.setServerConfig(serverConfig);
		repositoryManager.saveAppConfig();
	}

	@Override
	public String getLog() throws FileNotFoundException, IOException {
		logReader = new LogReader();
		return logReader.readLog();
	}

	@Override
	public ServerConfig getServerConfig() {
		return repositoryManager.getServerConfig();
	}

	@Override
	public RepositoryConfig getRepositoryConfig() {
		return repositoryManager.getRepositoryConfig();
	}
	


}

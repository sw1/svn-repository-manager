package beans;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;

import exceptions.BadTokenException;
import exceptions.NoUserException;
import permissions.User;

@Remote
public interface UserManagerRemote {

	public void mergeUser(User users) throws FileNotFoundException, IOException;
	public User getUser(String name);
	public List<User> getUsers();
	public List<User> getUserByCriterium(String name, String alias);
	public void createUser(User user) throws FileNotFoundException, IOException, Exception;
	public void createUsers(List<User> user) throws FileNotFoundException, IOException, Exception;
	public User getCurrentUser();
	public void sendResetToken(User user) throws NoUserException;
	public void mergeResetUser(User user) throws BadTokenException;
	public void  mergeUsers(List<User> users) throws FileNotFoundException, IOException;
	public void remove(User user);
}

package beans;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.annotation.security.RunAs;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.jboss.logging.Logger;

import auth.KeyAuth;
import auth.ResetAuth;
import config.RepositoryManager;
import exceptions.BadTokenException;
import exceptions.NoUserException;
import permissions.User;

/**
 * Session Bean implementation class UserManager
 */
@DeclareRoles({ "admin", "user", "manager" })
@Stateless
@LocalBean
public class UserManager implements UserManagerLocal, UserManagerRemote {

	@Resource
	public UserTransaction utx;
	@PersistenceUnit
	public EntityManagerFactory factory;
	@Resource
	private SessionContext context;

	@PersistenceContext
	private EntityManager em;

	@Inject
	private ResetAuth resetAuth;
	
	@Inject
	private KeyAuth keyAuth;
	
	@Inject
	private RepositoryManager repositoryManager;
	
	private static final Logger LOGGER = Logger.getLogger(ProjectManager.class);

	public UserManager() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void mergeUser(User user) throws FileNotFoundException, IOException {
		if (!user.getName().equals(context.getCallerPrincipal().getName()) && !context.isCallerInRole("admin")) {
			throw new SecurityException("User not permitted to change other users");
		}
		User oldUser = getUser(user.getName());
		if(oldUser!=null && user.getKey() !=null && user.getKey().equals(oldUser.getKey())==false){
			keyAuth.removeKeyfromAuthorized(oldUser);
			if(keyAuth.writeKey(user) == false){
				user.setKey("");
			}
		}
		em.merge(user);
		repositoryManager.writeAuthz();
	}

	@Override
	public User getUser(String name) {
		User u = null;
		Query q = em.createNamedQuery("selectUserName").setParameter("name", name);
		try{
			u = (User) q.getSingleResult();
		}
		catch(NoResultException e){
			LOGGER.info("No user \""+name+"\" found in db");
		}
		return u;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUsers() {
		List<User> users = null;
		Query q = em.createNamedQuery("selectAllUsers");
		users = q.getResultList();
		return users;
	}

	@Override
	public List<User> getUserByCriterium(String name, String alias) {
		Query q = em.createNamedQuery("selectUser").setParameter("name", name).setParameter("alias", alias);
		@SuppressWarnings("unchecked")
		List<User> u = q.getResultList();
		return u;
	}

	@RolesAllowed("admin")
	@Override
	public void createUser(User user) throws FileNotFoundException, IOException, Exception {
		em.persist(user);
		keyAuth.writeKey(user);
		keyAuth.createSystemUser(user);
		repositoryManager.getUsers().add(user);
		repositoryManager.writePasswd();
	}

	@Override
	public User getCurrentUser() {
		return getUser(context.getCallerPrincipal().getName());
	}

	@RolesAllowed("admin")
	@Override
	public void createUsers(List<User> users) throws FileNotFoundException, IOException, Exception {
		for (User u : users) {
			createUser(u);
		}
	}
	
	@RolesAllowed("admin")
	@Override
	public void mergeUsers(List<User> users) throws FileNotFoundException, IOException {
		for (User u : users) {
			mergeUser(u);
		}
	}
	
	public void mergeStartupUsers(List<User> users) throws FileNotFoundException, IOException {
		for (User u : users) {
			Query q = em.createNamedQuery("selectUserName").setParameter("name", u.getName());
			if(q.getResultList().size()>0){
				User dbUser = (User) q.getSingleResult();
				dbUser.setPassword(u.getPassword());
				dbUser.setAlias(u.getAlias());
				em.merge(dbUser);
			}else{
				em.merge(u);
			}
		}
	}

	@Override
	public void sendResetToken(User user) throws NoUserException {
		Query q = em.createNamedQuery("selectUserAndMail").setParameter("name", user.getName()).setParameter("mail",
				user.getMail());
		User foundUsuer = (User) q.getSingleResult();
		if (foundUsuer != null) {
			foundUsuer.setToken(resetAuth.sendResetToken(foundUsuer));
			em.merge(foundUsuer);
		} else {
			throw new NoUserException();
		}
	}

	@Override
	public void mergeResetUser(User user) throws BadTokenException {
		Query q = em.createNamedQuery("selectUserToken").setParameter("token", user.getToken());
		User dbUser = null;
		try {
			dbUser = (User) q.getSingleResult();
		} catch (javax.persistence.NoResultException e) {
			throw new BadTokenException();
		}
		if (user.getKey() != null) {
			dbUser.setKey(user.getKey());
		}
		if (user.getPassword() != null) {
			dbUser.setPassword(user.getPassword());
		}
		dbUser.setToken(null);
		em.merge(dbUser);
		repositoryManager.writeAuthz();
	}

	@Override
	public void remove(User user) {
		User dbUser = getUser(user.getName());
		em.remove(dbUser);
		repositoryManager.getUsers().remove(user);
		repositoryManager.writePasswd();
	}

}

package beans;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.ejb.Remote;
import javax.xml.bind.JAXBException;

import org.ini4j.InvalidFileFormatException;

import config.NoConfigurationException;
import config.RepositoryConfig;
import config.ServerConfig;

@Remote
public interface ConfigManagerRemote {

	public void readAppConfig() throws IOException, NoConfigurationException, JAXBException;
	public void readSVNConfig() throws InvalidFileFormatException, IOException;
	public void saveSVNConfig(RepositoryConfig repositoryConfig) throws FileNotFoundException, UnsupportedEncodingException;
	public void saveAppConfig(ServerConfig serverConfig) throws IOException, NoConfigurationException, JAXBException;
	public String getLog() throws FileNotFoundException, IOException;
	public ServerConfig getServerConfig();
	public RepositoryConfig getRepositoryConfig();
}

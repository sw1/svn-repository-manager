package beans;

import java.util.List;

import javax.ejb.Local;

import org.tmatesoft.svn.core.SVNException;

import permissions.Project;

@Local
public interface ProjectManagerLocal {

	public void createProject(Project project) throws SVNException;
	public List<Project> getProjectByCriterium(String path, String name) throws SVNException;
	public List<Project> getProjectsInPath(String path) throws SVNException;
	public void mergeProject(Project project) throws SVNException;
	public Project mergeSVNProject(Project project) throws SVNException;
}

package permissions;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;  
import javax.persistence.JoinTable;  

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.NamedQuery;
import javax.persistence.NamedQueries;

@NamedQueries({
	@NamedQuery(name = "selectGroup", query = "SELECT g FROM groups g WHERE g.name LIKE :name"),
	@NamedQuery(name = "selectAllGroups", query = "SELECT g FROM groups g"),
	@NamedQuery(name = "selectUserGroups", query = "SELECT g FROM groups g  JOIN g.users u WHERE u.name LIKE :name"),
	@NamedQuery(name = "selectUserGroupsNames", query = "SELECT g.name FROM groups g  JOIN g.users u WHERE u.name LIKE :name")
})
@Entity(name = "groups")
@Table(name = "groups")
public class Group implements Serializable{
	
	private static final long serialVersionUID = 844750368409677155L;
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	Long id;
	@Column(name = "name")
	String name;
	@Fetch(value = FetchMode.SUBSELECT)
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="users_groups", joinColumns=@JoinColumn(name="username", referencedColumnName="name"),
		      inverseJoinColumns={@JoinColumn(name="groupname", referencedColumnName="name")}) 
	@JsonManagedReference
	List<User> users;
	
	public Group(){
		
	}


	public Group(String name, List<User> users) {
		super();
		this.name = name;
		this.users = users;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	
	
}

package permissions;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@NamedQueries({
		@NamedQuery(name = "selectUserName", query = "SELECT u FROM users u WHERE u.name LIKE :name"),
		@NamedQuery(name = "selectUser", query = "SELECT u FROM users u WHERE u.name LIKE :name OR u.alias LIKE :alias"),
		@NamedQuery(name = "selectAllUsers", query = "SELECT u FROM users u"),
		@NamedQuery(name = "selectUserAndMail", query = "SELECT u FROM users u WHERE u.name = :name AND u.mail = :mail"),
		@NamedQuery(name = "selectUserToken", query = "SELECT u FROM users u WHERE u.token = :token")
})
@Entity(name = "users")
@Table(name = "users")
public class User implements Serializable{

	private static final long serialVersionUID = -7892018004482257999L;
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	Long id;
	@Column(name = "name")
	private String name;
	@Column(name = "mail")
	private String mail;
	@Column(name = "password")
	@JsonIgnore
	private String password;
	@Column(name = "alias")
	private String alias;
	@Column(name = "key", length=1024)
	private String key;
	@Column(name = "token")
	private String token;
	@ElementCollection(fetch=FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@Column(name = "roles")
	private List<String> roles;

	public User() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}

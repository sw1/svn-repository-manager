package permissions;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "permissions")
public class Permission implements Serializable{

	private static final long serialVersionUID = -8053892872690128066L;

	public enum TYPE{READ,NONE,READWRITE};
	
	@Id
	@Column(name = "id")
	@GeneratedValue
	private Long id;
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User user;
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "group_id")
	private Group group;
	@Column(name = "type")
	private TYPE access;
	@Column(name = "inverse")
	boolean inverse = false;
	
	static public TYPE getEnum(String permissionType){
		TYPE type = TYPE.NONE;
		if(permissionType==null){
			type = TYPE.NONE;
		}else if(permissionType.equalsIgnoreCase("r")||permissionType.equalsIgnoreCase("read")){
			type=TYPE.READ;
		}else if(permissionType.equalsIgnoreCase("w") || permissionType.equalsIgnoreCase("write")){
			type=TYPE.READWRITE;
		}else if (permissionType.equalsIgnoreCase("rw")){
			type=TYPE.READWRITE;
		}
		return type;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Group getGroup() {
		return group;
	}
	public void setGroup(Group group) {
		this.group = group;
	}
	public TYPE getAccess() {
		return access;
	}
	public void setAccess(TYPE access) {
		this.access = access;
	}
	public boolean isInverse() {
		return inverse;
	}
	public void setInverse(boolean inverse) {
		this.inverse = inverse;
	}
	
	public static String getAccessNameAuthz(TYPE type){
		String perm="";
		if(type==TYPE.READ ){
			perm = "r";
		} else if  (type==TYPE.READWRITE){
			perm = "rw";
		}
		return perm;
	}
	
	public static String getAccessNameConfiguration(TYPE type){
		String perm="none";
		if(type==TYPE.READ ){
			perm = "read";
		} else if  (type==TYPE.READWRITE){
			perm = "write";
		}
		return perm;
	}
	
	
}

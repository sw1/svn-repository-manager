package permissions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;

@NamedQueries({
	@NamedQuery(name = "selectProjectPathName", query = "SELECT p FROM projects p WHERE p.name = :name AND p.path = :path"),
	@NamedQuery(name = "selectProjectById", query = "SELECT p FROM projects p WHERE p.id = :id")
})
@Entity(name = "projects")
@Table(name = "projects")
public class Project implements Serializable {

	private static final long serialVersionUID = -296641866305145762L;

	@Id
	@GeneratedValue
	@Column(name = "id")
	Long id;
	@Column(name = "path", length = 4000)
	String path;
	@Column(name = "name")
	String name = "";
	@Transient
	List<Permission> permissions = new ArrayList<Permission>();
	@Transient
	Project root;
	@Transient
	@JsonIgnore
	List<Project> leafs = new ArrayList<Project>();
	@OneToMany(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	List<User> managers = new ArrayList<User>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<User> getManagers() {
		return managers;
	}

	public void setManagers(List<User> managers) {
		this.managers = managers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

	public Project getRoot() {
		return root;
	}

	public void setRoot(Project root) {
		this.root = root;
	}

	@JsonIgnore
	@Transient
	public List<Project> getLeafs() {
		return leafs;
	}

	public void setLeafs(List<Project> leafs) {
		this.leafs = leafs;
	}

	public Project searchLeaf(String name) {
		Project searched = null;
		if (name.equals("")) {
			searched = this;
		} else {
			for (Project p : leafs) {
				if (p.getName().equals(name)) {
					searched = p;
					break;
				}
			}
		}
		return searched;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Project other = (Project) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		return true;
	}

}

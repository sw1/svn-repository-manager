package exceptions;

public class BadTokenException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1532568778113924653L;

	public BadTokenException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BadTokenException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public BadTokenException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public BadTokenException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public BadTokenException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}

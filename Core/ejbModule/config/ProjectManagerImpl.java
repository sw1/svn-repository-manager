package config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import org.tmatesoft.svn.core.ISVNDirEntryHandler;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNCommitClient;
import org.tmatesoft.svn.core.wc.SVNLogClient;
import org.tmatesoft.svn.core.wc.SVNRevision;

import beans.ProjectManagerLocal;
import beans.UserManagerLocal;
import permissions.Project;
import permissions.User;

@SessionScoped
public class ProjectManagerImpl {
	@Inject
	private RepositoryManager repositoryManager;
	
	@Inject
	private PermissionsUtils permissionsUtils;
	@Inject
	SVNClientFacade svnClient;
	
	private final String commitMessage = "Created by SVNManager";

	private Project parentProject;
	
	public ProjectManagerImpl(){
	}
	
	public Project loadProjects(final String path) throws  SVNException {
		parentProject = null;
		final String realPath= path;
		SVNClientManager clientManager = svnClient.getSVNClientManager();
		SVNLogClient logClient = clientManager.getLogClient();
		SVNURL url;
		try {
			url = SVNURL.fromFile(new File(repositoryManager.getServerConfig().getSvnDirecory()+path));
			System.out.println("loadProjects path "+url.getPath());
			ISVNDirEntryHandler entryHandler = new ISVNDirEntryHandler() {

				@Override
				public void handleDirEntry(SVNDirEntry arg0)
						throws SVNException {
					if(arg0.getKind()==SVNNodeKind.DIR && arg0.getPath().isEmpty()==false){
						 permissionsUtils.addProject(realPath+"/"+arg0.getName());
					}

				}

			};
			ISVNDirEntryHandler parentEntryHandler = new ISVNDirEntryHandler() {

				@Override
				public void handleDirEntry(SVNDirEntry arg0)
						throws SVNException {
					if(arg0.getKind()==SVNNodeKind.DIR){
						parentProject = permissionsUtils.addProject(realPath);
					}

				}

			};
			logClient.doList(url, SVNRevision.HEAD, SVNRevision.HEAD, false,
					SVNDepth.EXCLUDE, SVNDirEntry.DIRENT_ALL, parentEntryHandler);
			logClient.doList(url, SVNRevision.HEAD, SVNRevision.HEAD, false,
					SVNDepth.IMMEDIATES, SVNDirEntry.DIRENT_ALL, entryHandler);
		} catch (SVNException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
		return parentProject;
	}
	
	public List<Project> getProjectByCriterium(String path, String name) throws SVNException {
		Project project = loadProjects(path);
		List<Project> matchedProjects = new ArrayList<>();
		for (Project prj: project.getLeafs()){
			if(prj.getName().contains(name)){
				matchedProjects.add(prj);
			}
		}
		return matchedProjects;
	}

	public Project getProject(String path) throws SVNException {
				
		return loadProjects(path);
	}

	public void createProject(Project project) throws SVNException {
		SVNClientManager svnClientManager = svnClient.getSVNClientManager();
		SVNCommitClient commitClient= svnClientManager.getCommitClient();
		SVNURL url[] = new SVNURL[1];
		url[0] = SVNURL.fromFile(new File(repositoryManager.getServerConfig().getSvnDirecory()+project.getPath()+project.getName()));
		commitClient.doMkDir(url, commitMessage);
	}
	
	public void removeProject(Project project) throws SVNException {
		SVNClientManager svnClientManager = svnClient.getSVNClientManager();
		SVNCommitClient commitClient= svnClientManager.getCommitClient();
		SVNURL url[] = new SVNURL[1];
		url[0] = SVNURL.fromFile(new File(repositoryManager.getServerConfig().getSvnDirecory()+project.getPath()+project.getName()));
		commitClient.doDelete(url, commitMessage);
		permissionsUtils.removeProject(project);
	}
	
}

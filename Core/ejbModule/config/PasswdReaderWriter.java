package config;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import permissions.User;

public class PasswdReaderWriter {
	
	public List<User> readUsers(String path){
		List<User> users = new ArrayList<User>();
		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(new File(path));
			
			while (fileScanner.hasNextLine()){
				   String line = fileScanner.nextLine();
				   line=line.split("#")[0];
				   if(line.contains("=")){
					   int split = line.indexOf("=");
					   String username=line.substring(0, split).trim();
					   String pass=line.substring(split+1).trim();
					   User u = new User();
					   u.setName(username);
					   u.setPassword(pass);
					   users.add(u);
				   }
				}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			fileScanner.close();
		}
		return users;
	}
	
	public void writePasswd(String path, List<User> users){
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(path, "UTF-8");
			writer.println("[users]");
			for(User u :users){
				writer.println(u.getName() + " = " + u.getPassword());
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			writer.close();
		}
		
	}
}

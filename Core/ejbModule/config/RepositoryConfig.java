package config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;
import org.jboss.logging.Logger;

import permissions.Group;
import permissions.Permission;
import permissions.Project;
import permissions.User;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RepositoryConfig implements Serializable {

	private static final long serialVersionUID = -3980915435574420602L;
	
	@XmlElement
	private Permission.TYPE anonAccess = Permission.TYPE.READ;
	@XmlElement
	private Permission.TYPE authAccess = Permission.TYPE.READWRITE;
	@XmlElement
	private String passwordDB = "passwd";
	@XmlElement
	private String authzDB = "authz";
	@XmlElement
	private String groupsDB = "groups";
	@XmlElement
	private String realm = "My First Repository";
	@XmlElement
	private String forceUsernameCase = "none";

	public Permission.TYPE getAnonAccess() {
		return anonAccess;
	}

	public void setAnonAccess(Permission.TYPE anonAccess) {
		this.anonAccess = anonAccess;
	}

	public Permission.TYPE getAuthAccess() {
		return authAccess;
	}

	public void setAuthAccess(Permission.TYPE authAccess) {
		this.authAccess = authAccess;
	}

	public String getPasswordDB() {
		return passwordDB;
	}

	public void setPasswordDB(String passwordDB) {
		this.passwordDB = passwordDB;
	}

	public String getAuthzDB() {
		return authzDB;
	}

	public void setAuthzDB(String authzDB) {
		this.authzDB = authzDB;
	}

	public String getGroupsDB() {
		return groupsDB;
	}

	public void setGroupsDB(String groupsDB) {
		this.groupsDB = groupsDB;
	}

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	public String getForceUsernameCase() {
		return forceUsernameCase;
	}

	public void setForceUsernameCase(String forceUsernameCase) {
		this.forceUsernameCase = forceUsernameCase;
	}

}

package config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.inject.Inject;

public class LogReader {

	@Inject
	private RepositoryManager repositoryManager;

	public String readLog() throws FileNotFoundException, IOException {
		String path = repositoryManager.getServerConfig().getSvnLogPath();
		String log = new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
		return log;
	}
}

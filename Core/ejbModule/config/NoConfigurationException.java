package config;

public class NoConfigurationException extends Exception {

	private static final long serialVersionUID = 4709571765974527292L;
	
	public NoConfigurationException(String string) {
		super(string);
	}

	public NoConfigurationException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NoConfigurationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public NoConfigurationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NoConfigurationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

}

package config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;
import org.jboss.logging.Logger;

import permissions.Group;
import permissions.Permission;
import permissions.Project;
import permissions.User;

@ApplicationScoped
public class AuthzReaderWriter {
	
	private static final Logger LOGGER = Logger.getLogger(AuthzReaderWriter.class);

	@Inject
	private PermissionsUtils permissionsUtils;

	public Project readAuthorization(String path, List<User> users, List<Group> groups, Project root) {
		Project newPrj = null;
		try {
			Wini ini = new Wini(new File(path));
			for (String projectPath : ini.keySet()) {
				if (!projectPath.equals("path") && !projectPath.equals("aliases") && !projectPath.equals("groups")) {
					String[] div = projectPath.split(":");
					String svnProjectPath = (div.length > 1) ? div[1] : div[0];
					newPrj = permissionsUtils.addProject(svnProjectPath);
					Ini.Section permissions = ini.get(projectPath);
					for (String perm : permissions.keySet()) {
						Permission newPerm = new Permission();
						if (perm.startsWith("~")) {
							newPerm.setInverse(true);
						}
						if (perm.startsWith("@")) {
							newPerm.setGroup(permissionsUtils.findGroup(perm.substring(1)));
						} else {
							newPerm.setUser(permissionsUtils.findUser(perm));
						}
						newPerm.setAccess(Permission.getEnum(permissions.get(perm)));
						newPrj.getPermissions().add(newPerm);
					}
				}
			}
		} catch (InvalidFileFormatException e) {
			LOGGER.error("Reading project authorization error - bad file format: "+path);
			LOGGER.trace(e.getStackTrace().toString());
			
		} catch (IOException e) {
			LOGGER.error("Reading project authorization error: "+path);
			LOGGER.trace(e.getStackTrace().toString());
		}
		return newPrj;
	}

	public void readAliases(String path, List<User> users) {
		try {
			Wini ini = new Wini(new File(path));
			Ini.Section section = ini.get("aliases");
			for (String alias : section.keySet()) {
				String usersStr = section.get(alias);
				User u=permissionsUtils.findUser(usersStr.trim());
				if(u!=null){
					u.setAlias(alias);
				}else{
					LOGGER.warn("No user \""+usersStr+ "\" in passwd for alias \""+alias+"\"");
				}
			}
		} catch (InvalidFileFormatException e) {
			LOGGER.error("Reading aliases error - bad file format: "+path);
			LOGGER.trace(e.getStackTrace().toString());
			
		} catch (IOException e) {
			LOGGER.error("Reading aliases error: "+path);
			LOGGER.trace(e.getStackTrace().toString());
		}
	}

	public List<Group> readGroups(String path, List<User> users) {
		ArrayList<Group> groups = new ArrayList<>();
		try {
			Wini ini = new Wini(new File(path));
			Ini.Section section = ini.get("groups");
			for (String group : section.keySet()) {
				ArrayList<User> groupUsers = new ArrayList<User>();
				String usersStr = section.get(group);
				for (String user : usersStr.split(",")) {
					groupUsers.add(permissionsUtils.findUser(user.trim()));
				}
				groups.add(new Group(group.trim(), groupUsers));
			}
		} catch (InvalidFileFormatException e) {
			LOGGER.error("Reading groups error - bad file format: "+path);
			LOGGER.trace(e.getStackTrace().toString());
		} catch (IOException e) {
			LOGGER.error("Reading groups error: "+path);
			LOGGER.trace(e.getStackTrace().toString());
		}

		return groups;
	}

	public void writeAuthz(String path, Project root, List<Group> groups, List<User> users) {
		List<Project> projects = permissionsUtils.getProjectList(root);
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(path, "UTF-8");
			writer.println("[aliases]");
			for (User u : users) {
				if (u.getAlias() != null) {
					writer.println(u.getName() + " = " + u.getAlias());
				}
			}
			writer.println();

			writer.println("[groups]");
			for (Group g : groups) {
				boolean first = true;
				writer.print(g.getName() + " = ");
				if(g.getUsers()!=null){
					for (User u : g.getUsers()) {
						if (first == true) {
							first = false;
						} else {
							writer.print(",");
						}
						if(u!=null){
							String name = u.getAlias() == null ? u.getName() : "&" + u.getAlias();
							writer.print(name);
						}
					}
				}
				writer.println();
			}
			writer.println();

			for (Project p : projects) {
				if(p.getPermissions().size()>0){
					writer.println("[" + p.getPath()+p.getName() + "]");
					for (Permission perm : p.getPermissions()) {
						String symbolicPerm = Permission.getAccessNameAuthz(perm.getAccess());
						if (perm.getGroup() != null) {
							writer.println("@" + perm.getGroup().getName() + " = " + symbolicPerm);
						} else {
							writer.println(perm.getUser().getName() + " = " + symbolicPerm);
						}
					}
				}
			}

		} catch (FileNotFoundException e) {
			LOGGER.error("Reading authz error - file not found: "+path);
			LOGGER.trace(e.getStackTrace().toString());
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("Reading authz error - unsupported encoding: "+path);
			LOGGER.trace(e.getStackTrace().toString());
		} finally {
			writer.close();
		}
	}
}

package config;

import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;

import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;

import beans.UserManagerLocal;
import permissions.User;

@SessionScoped
public class SVNClientFacade {
	private static SVNClientManager clientManager;

	@EJB
	UserManagerLocal userManager;

	public SVNClientManager getSVNClientManager() {
		if (clientManager == null) {
			User user = userManager.getCurrentUser();
			DefaultSVNOptions doptions = new DefaultSVNOptions();
			if(user!=null){
				clientManager = SVNClientManager.newInstance(doptions, user.getName(), user.getPassword());
			}else{
				clientManager = SVNClientManager.newInstance(doptions);
			}
		}
		return clientManager;
	}
}

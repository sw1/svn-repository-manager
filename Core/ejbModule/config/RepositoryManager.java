package config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.security.RunAs;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;
import org.jboss.logging.Logger;

import beans.UserManagerLocal;
import permissions.Group;
import permissions.Permission;
import permissions.Project;
import permissions.User;

@Named
@ApplicationScoped
@RunAs("admin")
public class RepositoryManager {

	private final String APP_DIR_NAME = "svnmanager";
	private final String CONFIG_NAME = "svnmanager_config.xml";
	private final Project root = new Project();
	private List<Group> groups = new ArrayList<>();
	private List<User> users = new ArrayList<>();

	private ServerConfig serverConfig = new ServerConfig();
	private static final Logger LOGGER = Logger.getLogger(RepositoryManager.class);

	private RepositoryConfig repositoryConfig = new RepositoryConfig();

	private String startupErrors;

	@Inject
	private AuthzReaderWriter authzReader;

	@PostConstruct
	public void initialize() {
		root.setPath("/");
	}

	public void loadAllConfigs() {
		LOGGER.info("Starting up application");
		try {
			readAppConfig();
			readSVNConfig();
		} catch (IOException e) {
			LOGGER.trace("Loading configuration IOException: \n" + e.getStackTrace().toString());
		} catch (NoConfigurationException e) {
			LOGGER.trace("Loading configuration NoConfigurationException: \n" + e.getStackTrace().toString());
		} catch (JAXBException e) {
			LOGGER.trace("Loading configuration JAXBException: \n" + e.getStackTrace().toString());
		}
		LOGGER.info("Loaded configuration");
	}

	public void readSVNConfig() throws InvalidFileFormatException, IOException {
		String path = serverConfig.getSvnservePath();
		Wini ini = new Wini(new File(path));
		Ini.Section general = ini.get("general");
		repositoryConfig.setAnonAccess(permissions.Permission.getEnum(general.get("anon-access")));
		repositoryConfig.setAuthAccess(permissions.Permission.getEnum(general.get("auth-access")));
		repositoryConfig.setPasswordDB(general.get("password-db"));
		repositoryConfig.setAuthzDB(general.get("authz-db"));
		repositoryConfig.setGroupsDB(general.get("groups-db"));
		repositoryConfig.setRealm(general.get("realm"));
		repositoryConfig.setForceUsernameCase(general.get("force-username-case"));
	}

	public void loadSVNObjects() {
		PasswdReaderWriter passwdReader = new PasswdReaderWriter();
		List<User> users = passwdReader
				.readUsers(getServerConfig().getSvnDirecory() + "/conf/" + getRepositoryConfig().getPasswordDB());
		getUsers().addAll(users);
		String authzPath = getServerConfig().getSvnDirecory() + "/conf/" + getRepositoryConfig().getAuthzDB();
		authzReader.readAliases(authzPath, users);
		List<Group> groups = authzReader.readGroups(authzPath, users);
		getGroups().addAll(groups);
		authzReader.readAuthorization(authzPath, users, groups, getRoot());
	}

	public String commentIfNull(String key, String value) {
		String line = "";
		if (value == null || value.isEmpty()) {
			line = "# " + key;
		} else {
			line = key + " = " + value;
		}
		return line;
	}

	public void writeSVNConfig() throws FileNotFoundException, UnsupportedEncodingException {
		String path = serverConfig.getSvnservePath();
		if (path == null) {
			throw new FileNotFoundException("SvnservePath not set");
		}
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(path, "UTF-8");
			writer.println("[general]");
			writer.println(commentIfNull("anon-access",
					Permission.getAccessNameConfiguration(repositoryConfig.getAnonAccess())));
			writer.println(commentIfNull("auth-access",
					Permission.getAccessNameConfiguration(repositoryConfig.getAuthAccess())));
			writer.println(commentIfNull("password-db", repositoryConfig.getPasswordDB()));
			writer.println(commentIfNull("authz-db", repositoryConfig.getAuthzDB()));
			writer.println(commentIfNull("groups-db", repositoryConfig.getGroupsDB()));
			writer.println(commentIfNull("realm", repositoryConfig.getRealm()));
			writer.println(commentIfNull("force-username-case", repositoryConfig.getForceUsernameCase()));
			writer.println();
			writer.println("[sasl]");
			writer.println("use-sasl = false");
			writer.println("min-encryption = 0");
			writer.println("max-encryption = 256");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

	public void writeAuthz() {
		authzReader.writeAuthz(getServerConfig().getSvnDirecory() + "/conf/" + getRepositoryConfig().getAuthzDB(), root,
				groups, users);
	}

	public void writePasswd() {
		PasswdReaderWriter passwdWriter = new PasswdReaderWriter();
		passwdWriter.writePasswd(getServerConfig().getSvnDirecory() + "/conf/" + getRepositoryConfig().getPasswordDB(),
				users);
	}

	private File getAppConfigFile() throws IOException, NoConfigurationException {
		String home = null;
		if (System.getenv("SVNMANAGER_HOME") != null) {
			home = System.getenv("SVNMANAGER_HOME");
		} else if (System.getenv("CATALINA_HOME") != null) {
			home = System.getenv("CATALINA_HOME") + "/" + APP_DIR_NAME;
		} else {
			home = APP_DIR_NAME;
		}
		File homedir = new File(home);
		if (!homedir.exists()) {
			if (!homedir.mkdirs()) {
				String error = "Couldn't create configuration directory: " + homedir.getAbsolutePath();
				LOGGER.error(error);
				throw new IOException(error);
			}
		}
		if (!homedir.isDirectory()) {
			String error = "Configuration path exists but not is a directory: " + homedir.getAbsolutePath();
			LOGGER.error(error);
			throw new IOException(error);
		}
		File configFile = new File(homedir.getAbsolutePath() + "/" + CONFIG_NAME);
		if (!configFile.exists()) {
			if (!configFile.createNewFile()) {
				String error = "Couldn't create configuration file: " + configFile.getAbsolutePath();
				LOGGER.error(error);
				throw new IOException(error);
			} else {
				String error = "Application doesn't have configuration";
				LOGGER.error(error);
				throw new NoConfigurationException(error);
			}
		}
		LOGGER.info("Used configuration file: " + configFile.getAbsolutePath());

		return configFile;
	}

	public void readAppConfig() throws IOException, NoConfigurationException, JAXBException {
		File configFile = getAppConfigFile();
		JAXBContext jaxbContext;
		jaxbContext = JAXBContext.newInstance(ServerConfig.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		serverConfig = (ServerConfig) jaxbUnmarshaller.unmarshal(configFile);
	}

	public void saveAppConfig() throws IOException, NoConfigurationException, JAXBException {
		File configFile = getAppConfigFile();
		JAXBContext jaxbContext;
		jaxbContext = JAXBContext.newInstance(ServerConfig.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(serverConfig, configFile);
		LOGGER.info("Saved app configuration file: " + configFile.getAbsolutePath());
	}

	public ServerConfig getServerConfig() {
		return serverConfig;
	}

	public void setServerConfig(ServerConfig serverConfig) {
		this.serverConfig = serverConfig;
	}

	public Project getRoot() {
		return root;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<User> mergeUsers(List<User> extUsers) {
		List<User> missingUsers = new ArrayList<>();
		int index = -1;
		for (User internalUser : this.users) {
			index = extUsers.indexOf(internalUser);
			if (index == -1) {
				missingUsers.add(internalUser);
				LOGGER.debug("User " + internalUser.getName() + " added to DB");
			} else {
				User externalUser = extUsers.get(index);
				internalUser.setKey(externalUser.getKey());
				internalUser.setPassword(externalUser.getPassword());
				internalUser.setRoles(externalUser.getRoles());
			}
		}
		for (User externalUser : extUsers) {
			if (!users.contains(externalUser)) {
				users.add(externalUser);
				LOGGER.debug("User " + externalUser.getName() + " added to passwd");
			}
		}
		LOGGER.info("Users from passwd and DB were merged");
		return missingUsers;
	}

	public void mergeGroups(List<Group> groups) {

	}

	public <T> List<T> getMissingsLists(List<T> firstList, List<T> secondList) {
		List<T> missingUsers = new ArrayList<>();
		int index = -1;
		for (T internalUser : secondList) {
			index = firstList.indexOf(internalUser);
			if (index == -1) {
				missingUsers.add(internalUser);
			}
		}
		return missingUsers;
	}

	public String getAPP_DIR_NAME() {
		return APP_DIR_NAME;
	}

	public String getCONFIG_NAME() {
		return CONFIG_NAME;
	}

	public String getStartuperrors() {
		return startupErrors;
	}

	public void setStartuperrors(String startupErrors) {
		this.startupErrors = startupErrors;
	}

	public RepositoryConfig getRepositoryConfig() {
		return repositoryConfig;
	}

	public void setRepositoryConfig(RepositoryConfig repositoryConfig) {
		this.repositoryConfig = repositoryConfig;
	}
}

package config;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.tmatesoft.svn.core.SVNException;

import beans.ProjectManagerLocal;
import permissions.Group;
import permissions.Project;
import permissions.User;

@Singleton
public class PermissionsUtils {

	@Inject
	private RepositoryManager repositoryManager;

	@EJB
	private ProjectManagerLocal projectManager;
	
	public User findUser(String name) {
		final List<User> users = repositoryManager.getUsers();
		for (User u : users) {
			if (u.getName().equals(name) || (u.getAlias()!=null && u.getAlias().equals(name))) {
				return u;
			}
		}
		return null;
	}

	public Group findGroup(String name) {
		final List<Group> groups = repositoryManager.getGroups();
		for (Group g : groups) {
			if (g.getName().equals(name)) {
				return g;
			}
		}
		return null;
	}

	public Project addProject(String projectPath) {
		Project root = repositoryManager.getRoot();
		Project newPrj = null;
		projectPath = projectPath.trim();
		projectPath= projectPath.replaceAll("//+", "/");
		projectPath =projectPath.substring(1);
		String subPrj[] = projectPath.split("/");
		
		String actualPath = "";
		for (String subPrjName : subPrj) {
			actualPath += "/" + subPrjName;
			Project p = root.searchLeaf(subPrjName);
			if (p == null) {
				newPrj = new Project();
				newPrj.setName(subPrjName);
				newPrj.setPath(actualPath.substring(0, actualPath.lastIndexOf("/")+1));
				if(root.getLeafs()==null){
					root.setLeafs(new ArrayList<Project>());
				}
				try {
					newPrj=projectManager.mergeSVNProject(newPrj);
				} catch (SVNException e) {
				}
				root.getLeafs().add(newPrj);
				root = newPrj;
			} else {
				root = p;
			}
		}
		return root;
	}
	
	public boolean removeProject(Project project){
		boolean removed = true;
		Project root = repositoryManager.getRoot();
		String subPrj[] = project.getPath().substring(1,project.getPath().length()).split("/");
		
		for (String subPrjName : subPrj) {
			Project p = root.searchLeaf(subPrjName);
			if (p == null) {
				removed = false;
			} else {
				root = p;
			}
		}
		if(removed){
			removed = root.getLeafs().remove(project);
		}
		return removed;
	}
	
	public List<Project> getProjectList(Project root) {
		List<Project> projectList = new ArrayList<Project>();
		for(Project project: root.getLeafs()){
			projectList.addAll(getProjectList(project));
		}
		projectList.add(root);
		return projectList;
	}
}

package config;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import permissions.Project;



@XmlRootElement(name = "server_config")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServerConfig implements Serializable{
	
	private static final long serialVersionUID = 1253756212335936738L;
		
	@XmlElement(name = "svn_directory")
	private String svnDirecory;
	@XmlElement(name = "svn_serve_path")
	private String svnservePath;
	@XmlElement(name = "svn_log_path")
	private String svnLogPath;
	@XmlElement(name = "server_host")
	private String serverHost;
	
	@XmlElement(name = "smtp_host")
	private String smtpHost;
	@XmlElement(name = "smtp_port")
	private String smtpPort;
	@XmlElement(name = "smtp_user")
	private String smtpUser;
	@XmlElement(name = "smtp_password")
	private String smtpPassword;
	@XmlElement(name = "smtp_from")
	private String smtpFrom;
	@XmlElement(name = "ssh_key_path")
	private String sshKeyPath;
	
	public ServerConfig(){
	}

	public String getSvnDirecory() {
		return svnDirecory;
	}

	public void setSvnDirecory(String svnDirecory) {
		this.svnDirecory = svnDirecory;
	}

	public String getSvnservePath() {
		return svnservePath;
	}

	public void setSvnservePath(String svnservePath) {
		this.svnservePath = svnservePath;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public String getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}

	public String getSmtpUser() {
		return smtpUser;
	}

	public void setSmtpUser(String smtpUser) {
		this.smtpUser = smtpUser;
	}

	public String getSmtpPassword() {
		return smtpPassword;
	}

	public void setSmtpPassword(String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}

	public String getSmtpFrom() {
		return smtpFrom;
	}

	public void setSmtpFrom(String smtpFrom) {
		this.smtpFrom = smtpFrom;
	}

	public String getSshKeyPath() {
		return sshKeyPath;
	}

	public void setSshKeyPath(String sshKeyPath) {
		this.sshKeyPath = sshKeyPath;
	}

	public String getSvnLogPath() {
		return svnLogPath;
	}

	public void setSvnLogPath(String svnLogPath) {
		this.svnLogPath = svnLogPath;
	}

	public String getServerHost() {
		return serverHost;
	}

	public void setServerHost(String serverHost) {
		this.serverHost = serverHost;
	}
	
}

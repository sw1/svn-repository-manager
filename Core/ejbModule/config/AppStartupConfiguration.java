package config;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.security.RunAs;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;
import org.tmatesoft.svn.core.SVNException;

import beans.GroupManagerLocal;
import beans.ProjectManagerLocal;
import beans.ProjectManagerRemote;
import beans.UserManagerLocal;
import permissions.Group;
import permissions.User;

@Startup
@Singleton
@Named
public class AppStartupConfiguration {

	private static final Logger LOGGER = Logger.getLogger(AppStartupConfiguration.class);

	@Inject 
	private RepositoryManager repositoryManager;

	@EJB
	private UserManagerLocal userManager;
	@EJB
	private GroupManagerLocal groupManager;
	private String errors = "";
	@EJB
	private ProjectManagerLocal projectManager;
	
	@PostConstruct
    public void onStartup() {
		repositoryManager.loadAllConfigs();
		repositoryManager.loadSVNObjects();
		List<User> users = userManager.getUsers();
		List<User> missingUsers = repositoryManager.mergeUsers(users);
		try {
			repositoryManager.writeSVNConfig();
		} catch (FileNotFoundException e) {
			LOGGER.trace(e.getStackTrace().toString());
			errors += e.getMessage() + "\n";
		} catch (UnsupportedEncodingException e) {
			LOGGER.trace(e.getStackTrace().toString());
			errors += e.getMessage() + "\n";
		}
		try {
			userManager.mergeStartupUsers(repositoryManager.getUsers());
		} catch (Exception e) {
			
		}
		List<Group> groups = groupManager.getGroups();
		List<Group> missingGroups = repositoryManager.getMissingsLists(groupManager.getGroups(), groups);
		groupManager.mergeStartupGroups(repositoryManager.getGroups());
		repositoryManager.setGroups(groupManager.getGroups());
		try {
			repositoryManager.writeSVNConfig();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(!errors.equals("")){
			LOGGER.error("Application startup errors: \n"+errors);
		}
    }

}


    create table User_roles (
        User_name varchar(255) not null,
        roles varchar(255)
    );

    create table groups (
        name varchar(255) not null,
        primary key (name)
    );

    create table groups_users (
        groups_name varchar(255) not null,
        users_name varchar(255) not null,
        unique (users_name)
    );

    create table permissions (
        id int8 not null,
        type int4,
        inverse boolean,
        group_id varchar(255),
        user_id varchar(255),
        primary key (id)
    );

    create table projects (
        path varchar(4000) not null,
        name varchar(255),
        root varchar(4000),
        primary key (path)
    );

    create table projects_permissions (
        projects_path varchar(4000) not null,
        permissions_id int8 not null,
        unique (permissions_id)
    );

    create table projects_projects (
        projects_path varchar(4000) not null,
        leafs_path varchar(4000) not null,
        unique (leafs_path)
    );

    create table projects_users (
        projects_path varchar(4000) not null,
        managers_name varchar(255) not null,
        unique (managers_name)
    );

    create table users (
        name varchar(255) not null,
        alias varchar(255),
        key varchar(255),
        mail varchar(255),
        password varchar(255),
        token varchar(255),
        primary key (name)
    );

    alter table User_roles 
        add constraint FKEA1475697E4FC594 
        foreign key (User_name) 
        references users;

    alter table groups_users 
        add constraint FKF4231BDDFE6D0177 
        foreign key (users_name) 
        references users;

    alter table groups_users 
        add constraint FKF4231BDD3A92D28B 
        foreign key (groups_name) 
        references groups;

    alter table permissions 
        add constraint FK4392F484F60DEB50 
        foreign key (group_id) 
        references groups;

    alter table permissions 
        add constraint FK4392F4846150C524 
        foreign key (user_id) 
        references users;

    alter table projects 
        add constraint FKC479187A3A1CA471 
        foreign key (root) 
        references projects;

    alter table projects_permissions 
        add constraint FKE885843F39CB904F 
        foreign key (permissions_id) 
        references permissions;

    alter table projects_permissions 
        add constraint FKE885843F22FB6459 
        foreign key (projects_path) 
        references projects;

    alter table projects_projects 
        add constraint FK4B201A9F19CB0DE 
        foreign key (leafs_path) 
        references projects;

    alter table projects_projects 
        add constraint FK4B201A9F22FB6459 
        foreign key (projects_path) 
        references projects;

    alter table projects_users 
        add constraint FK39C2460367D933D9 
        foreign key (managers_name) 
        references users;

    alter table projects_users 
        add constraint FK39C2460322FB6459 
        foreign key (projects_path) 
        references projects;

    create sequence hibernate_sequence;

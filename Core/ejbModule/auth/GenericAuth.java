package auth;

import permissions.User;

public interface GenericAuth {
	
	public void writePassword();
	public boolean comparePassword(User user);
}

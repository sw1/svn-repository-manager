package auth;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import permissions.User;
import config.RepositoryManager;
import config.ServerConfig;

@Named
@ApplicationScoped
public class ResetAuth {

	private SecureRandom random = new SecureRandom();

	@Inject
	private RepositoryManager repositoryManager;

	public String generateToken() {
		return new BigInteger(130, random).toString(32);
	}

	public String sendResetToken(User user) {
		ServerConfig serverConfig = repositoryManager.getServerConfig();
		Properties props = new Properties();
		String token = generateToken();
	    props.put("mail.smtp.host", serverConfig.getSmtpHost());
	    props.put("mail.from", serverConfig.getSmtpFrom());
	    props.put("mail.protocol.port", serverConfig.getSmtpPort());
	    Session session = Session.getInstance(props, null);
	    session.setPasswordAuthentication(new URLName(serverConfig.getSmtpHost()), new PasswordAuthentication(serverConfig.getSmtpUser(), serverConfig.getSmtpPassword()));
		Message message = new MimeMessage(session);
		user.setToken(token);
		try {
			message.setFrom();
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(user.getName(), false));
			message.setSubject("Password reset");
			message.setText("To reset tour password visit: "
					+ serverConfig.getServerHost() + "/#/reset?token="
					+ user.getToken()+"\nReset token:\n"+user.getToken());
			message.setHeader("X-Mailer", "SVNManager");
			message.setSentDate(new Date());
			Transport.send(message);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return token;

	}
}

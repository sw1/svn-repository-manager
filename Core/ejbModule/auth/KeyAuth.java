package auth;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.Scanner;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import config.RepositoryManager;
import permissions.User;

@Named
@ApplicationScoped
public class KeyAuth {

	@Inject
	private RepositoryManager repositoryManager;

	private static final Logger LOGGER = Logger.getLogger(KeyAuth.class);

	public void createSystemUser(User user) throws InterruptedException, IOException, Exception {
		try{
			//username ALL= NOPASSWD:/usr/bin/useradd
			String command = "sudo useradd --no-create-home --system " + user.getName();
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec(command);
			process.waitFor();
			BufferedReader bufferReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line = new String();
			String output = new String();
			while ((line = bufferReader.readLine()) != null) {
				output += line;
			}
			bufferReader.close();
			if (process.exitValue() != 0) {
				LOGGER.error("Error adding system user: " + user.getName());
				throw new Exception(output);
			}
		}catch(InterruptedException e){
			LOGGER.error("Error adding system user: " + user.getName());
			LOGGER.trace(e.getStackTrace().toString());
			throw e;
		}
	}

	public boolean writeKey(User user) {
		boolean written = true;
		String path = repositoryManager.getServerConfig().getSshKeyPath();
		if (user.getKey() != null && user.getKey().isEmpty() == false && keyIsAuthorized(user) == false
				&& validateKey(user)) {
			PrintWriter writer = null;
			try {
				writer = new PrintWriter(new BufferedWriter(new FileWriter(path, true)));
				writer.println(user.getKey());
			} catch (FileNotFoundException e) {
				LOGGER.error("Error writing ssh key - file not found: " + path);
				LOGGER.trace(e.getStackTrace().toString());
				written = false;
			} catch (IOException e) {
				LOGGER.error("Error writing ssh key:" + path);
				LOGGER.trace(e.getStackTrace().toString());
				written = false;
			} finally {
				writer.close();
			}
		} else {
			written = false;
		}
		return written;
	}

	public boolean keyIsAuthorized(User user) {
		boolean hasKey = false;
		String path = repositoryManager.getServerConfig().getSshKeyPath();
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File(path));
			while (scanner.hasNextLine()) {
				if (scanner.nextLine().contains(user.getKey())) {
					hasKey = true;
					break;
				}
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			LOGGER.error("Error checking ssh key - file not found: " + path);
			LOGGER.trace(e.getStackTrace().toString());
		} 
		return hasKey;
	}

	public void removeKeyfromAuthorized(User user) throws FileNotFoundException, IOException {
		String path = repositoryManager.getServerConfig().getSshKeyPath();
		try {
			File tmp = File.createTempFile("tmp", "");
			File authFile = new File(path);

			BufferedReader br;

			br = new BufferedReader(new FileReader(authFile));
			BufferedWriter bw = new BufferedWriter(new FileWriter(tmp));

			String line;
			while (null != (line = br.readLine())) {
				if (!line.contains(user.getKey())) {
					bw.write(line + "\n");
				}
			}
			br.close();
			bw.close();

			if (authFile.delete()){
				tmp.renameTo(authFile);
			}
		} catch (FileNotFoundException e) {
			LOGGER.error("Error removing ssh key - file not found: " + path);
			LOGGER.trace(e.getStackTrace().toString());
			throw e;
		} catch (IOException e) {
			LOGGER.error("Error FileNotFoundException ssh key: " + path);
			LOGGER.trace(e.getStackTrace().toString());
			throw e;
		}
	}

	public boolean validateKey(User user) {
		boolean valid = true;
		File tmp = null;
		try {
			tmp = File.createTempFile("tmp", "");
			BufferedWriter bw = new BufferedWriter(new FileWriter(tmp));
			bw.write(user.getKey());
			bw.flush();
			bw.close();

			String command = "ssh-keygen -l -f " + tmp.getAbsolutePath();
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec(command);
			process.waitFor();
			BufferedReader inputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line = new String();
			String output = new String();
			while ((line = inputReader.readLine()) != null) {
				output += line;
			}
			inputReader.close();
			if (process.exitValue() != 0) {
				LOGGER.error("Bad key\n" + user.getKey() + "\n" + output);
				valid = false;
			}
			tmp.delete();
		} catch (IOException e) {
			LOGGER.error("Error with temp file: " + tmp.getAbsolutePath());
			LOGGER.trace(e.getStackTrace().toString());
			valid = false;
		} catch (InterruptedException e) {
			LOGGER.error("Validating key exception");
			LOGGER.trace(e.getStackTrace().toString());
			valid = false;
		}
		return valid;
	}
}
